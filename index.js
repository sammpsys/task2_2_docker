const express = require("express")
const { dirname } = require("path")
const app = express()

const {PORT=3000}=process.env
const path = require("path")

//middleware allow all static content to be server to client
app.use( express.static(path.join(__dirname,"public")))

app.get("/", function (req,res) {
    return res.sendFile( path.join( __dirname, "public", "index.html" ) );
})

app.get("/goodbye", function (req,res) {
    return res.send("Goodbye from awesome server!")
})

app.listen(PORT, function() {
    console.log("Server has started")
})

