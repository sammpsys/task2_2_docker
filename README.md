# Task2_2_docker
Contains a dockerfile that uses a node.js image.
## Table of Contents

- [Requirements](#requirements)
- [Setup](#setup)
- [Docker questions](#Docker)
- [Contributors](#contributers)

## Requirements

Requires 'Docker'.

## Setup
```sh
    # You can either clone the repo and build the dockerfile:
    $ docker build -t sam-docker:1.0 .
    # Or you can build directly from the repo:
    $ docker build https://gitlab.com/sammpsys/task2_2_docker.git#main -t sam-docker:1.0
    # Then it can be run
    $ docker run -p 8080:3000 sam-docker:1.0
```

## Docker questions
1. A unique use of Docker is that it provides support for machine learning applications. It also simplifies ML operations by allowing them to be used in tandem with Kubernetes.
2. An interesting use case of docker containers are Jupyter Docker Stacks. They can be run using Docker, and are used in Machine Learning and Data Science. Link : https://github.com/jupyter/docker-stacks
3. One use of Docker not discussed in class is that it can be used for data science and statistics. There is an image called RStudio that can be used for this purpose.


## Contributors
[Sam Nassiri @sammpsys](https://gitlab.com/sammpsys)

